from itemid import itemid

import pytest

url = itemid()

def test_unshorten():
    assert int('0', 36) == url.unshorten('0')
    assert int('z', 36) == url.unshorten('z')
    assert int('foobar', 36) == url.unshorten('foobar')

def test_shorten():
    assert url.shorten(0) == '0'
    assert url.shorten(10) == 'a'
    assert url.shorten(35) == 'z'
    assert url.shorten(36) == '10'

    foobar = int('foobar', 36)
    assert url.shorten(foobar) == 'foobar'


def test_unique_code():
    with pytest.raises(AssertionError) as e:
        url = itemid('aaaabbbbbcccc')

    assert 'duplicate' in str(e.value)
