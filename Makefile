genctags:
	@@ctags -R --extra=+f --exclude=flakes --exclude=.git

pep8:
	@@autoflake -ri --remove-all-unused-imports eight
	@@autopep8 -ri --max-line-length 110 eight

clear:
	@@find . -name '*.pyc' | xargs rm
	@@find . -name '__pycache__' | xargs rm -rf

package:
	python setup.py sdist

upload:
	twine upload dist/*.gz
